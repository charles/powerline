# Dutch translation of powerline debconf templates.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the powerline package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: powerline_2.7-1\n"
"Report-Msgid-Bugs-To: powerline@packages.debian.org\n"
"POT-Creation-Date: 2018-07-24 06:14+0200\n"
"PO-Revision-Date: 2019-01-14 21:04+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.7\n"

#. Type: title
#. Description
#: ../powerline.templates:1001
msgid "powerline: Setup"
msgstr "powerline: Instelling"

#. Type: multiselect
#. Description
#: ../powerline.templates:2001
msgid "Enable powerline globally?"
msgstr "powerline algemeen inschakelen?"

#. Type: multiselect
#. Description
#: ../powerline.templates:2001
msgid ""
"powerline can be enabled globally for all users of certain applications on "
"this system."
msgstr ""
"powerline kan algemeen ingeschakeld worden voor alle gebruikers van bepaalde "
"toepassingen op dit systeem."
